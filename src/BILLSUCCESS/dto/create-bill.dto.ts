import { IsNotEmpty, Length } from 'class-validator';
export class CreateBillDto {
  invoice: string;

  // @IsNotEmpty()
  // @Length(8, 32)
  namepd: string;

  // @IsNotEmpty()
  // @Length(8, 32)
  amountbill: number;

  units: string;

  priceperunits: number;

  discount: number;

  price: number;

  time: string;

  timepay: string;

  status: string;

  isChecked?: boolean;
}
