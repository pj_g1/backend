import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';
@Entity()
export class Promotion {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name: string;

  @Column()
  discount: number;

  @Column()
  type: string;
}
