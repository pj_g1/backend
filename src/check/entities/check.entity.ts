import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';
@Entity()
export class Check {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  fullname: string;

  @Column()
  password: string;

  @Column()
  timein: string;

  @Column()
  timeout: string;
}
