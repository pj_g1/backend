import { IsNotEmpty, Length } from 'class-validator';
export class CreateCheckDto {
  fullname: string;
  password: string;
  timein: string;
  timeout: string;
}
