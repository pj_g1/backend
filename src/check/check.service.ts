import { Injectable } from '@nestjs/common';
import { CreateCheckDto } from './dto/create-check.dto';
import { UpdateCheckDto } from './dto/update-check.dto';
import { Check } from './entities/check.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

@Injectable()
export class CheckService {
  constructor(
    @InjectRepository(Check)
    private checkRepository: Repository<Check>,
  ) {}

  create(createCheckDto: CreateCheckDto): Promise<Check> {
    return this.checkRepository.save(createCheckDto);
  }

  findAll(): Promise<Check[]> {
    return this.checkRepository.find();
  }

  findOne(id: number) {
    return this.checkRepository.findOneBy({ id });
  }

  async update(id: number, updateCheckDto: UpdateCheckDto) {
    await this.checkRepository.update(id, updateCheckDto);
    const check = await this.checkRepository.findOneBy({ id });
    return check;
  }

  async remove(id: number) {
    const deleteCheck = await this.checkRepository.findOneBy({ id });
    return this.checkRepository.remove(deleteCheck);
  }
}
