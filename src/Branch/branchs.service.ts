import { Injectable } from '@nestjs/common';
import { CreateBranchDto } from './dto/create-Branch.dto';
import { UpdateBranchDto } from './dto/update-Branch.dto';
import { Branch } from './entities/Branch.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

@Injectable()
export class BranchsService {
  constructor(
    @InjectRepository(Branch)
    private branchsRepository: Repository<Branch>,
  ) {}

  create(createBranchDto: CreateBranchDto): Promise<Branch> {
    return this.branchsRepository.save(createBranchDto);
  }

  findAll(): Promise<Branch[]> {
    return this.branchsRepository.find();
  }

  findOne(id: number) {
    return this.branchsRepository.findOneBy({ id });
  }

  async update(id: number, updateBranchDto: UpdateBranchDto) {
    await this.branchsRepository.update(id, updateBranchDto);
    const branch = await this.branchsRepository.findOneBy({ id });
    return branch;
  }

  async remove(id: number) {
    const deleteBranch = await this.branchsRepository.findOneBy({ id });
    return this.branchsRepository.remove(deleteBranch);
  }
}
