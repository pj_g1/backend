import { Module } from '@nestjs/common';
import { BranchsService } from './branchs.service';
import { BranchsController } from './branchs.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Branch } from './entities/Branch.entity';

@Module({
  imports: [TypeOrmModule.forFeature([Branch])],
  controllers: [BranchsController],
  providers: [BranchsService],
})
export class BranchModule {}
