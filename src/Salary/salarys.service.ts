import { Injectable } from '@nestjs/common';
import { CreateSalaryDto } from './dto/create-salary.dto';
import { UpdateSalaryDto } from './dto/update-salary.dto';
import { Salary } from './entities/salary.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

@Injectable()
export class SalarysService {
  constructor(
    @InjectRepository(Salary)
    private salarysRepository: Repository<Salary>,
  ) {}

  create(createBillDto: CreateSalaryDto): Promise<Salary> {
    return this.salarysRepository.save(createBillDto);
  }

  findAll(): Promise<Salary[]> {
    return this.salarysRepository.find();
  }

  findOne(id: number) {
    return this.salarysRepository.findOneBy({ id });
  }

  async update(id: number, updateBillDto: UpdateSalaryDto) {
    await this.salarysRepository.update(id, updateBillDto);
    const bill = await this.salarysRepository.findOneBy({ id });
    return bill;
  }

  async remove(id: number) {
    const deleteBill = await this.salarysRepository.findOneBy({ id });
    return this.salarysRepository.remove(deleteBill);
  }
}
