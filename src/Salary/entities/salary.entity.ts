import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class Salary {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  date: string;

  @Column()
  fullname: string;

  @Column()
  workinghour: number;

  @Column()
  workrate: number;

  @Column()
  status: string;

  @Column()
  salary: number;
}
