import { IsNotEmpty, Length } from 'class-validator';
export class CreateSalaryDto {
  date: string;

  fullname: string;

  workinghour: number;

  workrate: number;

  status: string;

  salary: number;
}
